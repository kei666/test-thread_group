# https://cmake.org/cmake/help/v3.0/module/FindBoost.html

cmake_minimum_required(VERSION 3.5.1 FATAL_ERROR)


if (USE_STATIC_BOOST)
    set(Boost_USE_STATIC_LIBS ON)
endif ()

set(Boost_MULTITHREADED ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

find_package(Boost REQUIRED COMPONENTS program_options thread system timer)

if (Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
    link_directories (${Boost_LIBRARY_DIRS})

    message(STATUS "Boost version: ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION}")
    message(STATUS "Boost include dir: ${Boost_INCLUDE_DIR}")
    message(STATUS "Boost include dirs: ${Boost_INCLUDE_DIRS}")
    message(STATUS "Boost library dir: ${BOOST_LIBRARYDIR}")
    message(STATUS "Boost libraries: ${Boost_LIBRARIES}")
else()
    message(WARNING "Could not find Boost.")
endif()