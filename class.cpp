#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>


class THGROUP {
public:
    THGROUP(int t){
        this->loop_num = t;
    }

    ~THGROUP(){}

    void loop(int t) {
        for (int i = 0; i < this->loop_num; i++) {
            int x[length] = {};
            for (int j = 0; j < this->length; j++) {
                x[j] += t;
            }
        }
    }

    void th_loop(int t) {
        boost::thread_group th_group;
        for (int i = 0; i < this->loop_num; i++) {
            th_group.create_thread(boost::bind(&THGROUP::th,
                                               this,
                                               t
            ));
        }
        th_group.join_all();
    }

private:
    int loop_num;
    const int length = 1000000;
    void th(int t) {
        int x[length] = {}; //すべての要素を０に初期化
        for (int i = 0; i < this->length; i++) {
            x[i] += t;
        }
    }
};


int main(int argc, char **argv) {
    THGROUP c = THGROUP(1000);

    boost::timer::cpu_timer timer;
    c.loop(2);
    std::cout << timer.format() << std::endl;

    timer.start();
    c.th_loop(2);
    std::cout << timer.format() << std::endl;

    return EXIT_SUCCESS;
}